# novel

- title: クワガタにチョップしたらタイムスリップした
- title_zh: 給鍬形蟲一記手刀結果穿越時空了
- author: タカハシ ヨウ
- illust:
- source: http://q.dmzj.com/1402/index.shtml
- cover: http://bkmkn.s3-website-ap-northeast-1.amazonaws.com/9784063764635/9784063764635_w.jpg
- publisher:
- date: 2013-03-11T15:09:04+08:00
- status: 已完结
- novel_status: 0x0301

## authors

- 家の裏でマンボウが死んでるP
- 翻車魚P

## illusts

- 下田　将也
- 竜宮 ツカサ
- 

## publishers

- dmzj

## series

- name: 给锹形虫一记手刀结果穿越时空了

## preface


```
天空的顏色毫無生氣，違反物理法則的奇怪建築物。
在陌生的街道中，回過神來，我淡路奈津美正與一台殺戮機器（暫）對峙著。
明明只是在和我的寵物鍬形蟲一起玩耍而已，到底怎麼回事嘛！
我不明所以地跑著。這裡是哪裡？為什麼我會在這裡？回想一下。
2016年7月21日——非常想讀檔重來，我人生中最糟糕的一天。
```

## tags

- node-novel
- dmzj
- 冒险
- 奇幻
- 日本
- 穿越
- Vocaloid
- 時空穿越
- 時空
- 

# contribute

- 妹痕
- 

# options

## dmzj

- novel_id: 1402

## downloadOptions

- noFilePadend: true
- filePrefixMode: 4
- startIndex: 1

## textlayout

- allow_lf2: true

# link

- https://zh.moegirl.org/zh-tw/%E7%BB%99%E9%94%B9%E5%BD%A2%E8%99%AB%E4%B8%80%E8%AE%B0%E6%89%8B%E5%88%80%E7%BB%93%E6%9E%9C%E7%A9%BF%E8%B6%8A%E6%97%B6%E7%A9%BA%E4%BA%86
- http://kodansha-box.jp/topics/kuwaccho/index.html
- https://www.youtube.com/watch?v=qtcvZ9kwJzQ
- https://tw.manhuagui.com/comic/10154/
- 

