第二章開始


════════════════════

回到家，見到的是一年前召喚過的魔人夏緹亞在我家理所當然地放鬆的樣子。

夏緹亞是在幫我一起製作影耶的女性魔人。
影耶本來只是我趁著醉頭做出來的人造人美少女，卻因為這貨的緣故不知道為什麼變我變成美少女了。嘛，現在還挺享受的所以就算了。

「那個⋯⋯⋯恩，好久不見。過了一年多嗎？不對，夏緹亞看來可能⋯⋯」
「叫媽媽」
「⋯⋯母親看來也是一年嗎」
「是唷，從我這邊來看也大概是一年，你過得如何？」

夏緹亞有精神就好。
她似乎把和我（陰矢）一起做的影耶當作女兒什麼之類的，一直想讓我（影耶）稱呼自己為媽媽。說實話這想法我是不明白而且又羞恥所以我是希望不要這樣，但不這樣叫夏緹亞就會鬧別扭，所以某種程度上還是妥協了。

「過得算是不錯，就是有點⋯⋯不對，是有一大堆問題」
「嘿？」

我把和勇者・鈴木光彌相遇之後的事情簡略說明一遍。

光彌說要和我一起回到原本的世界，為了證明影耶比光彌更強而戰鬥卻因為神聖魔法而輸了。然後魔王只要有設置在障壁塔上的隔絶褉石就是無敵的，然後那個隔絶褉石不是勇者的聖劍就沒法破壊，而勇者不死聖劍就不能換人用，為了搶走聖劍在障壁塔布下天羅地網埋伏卻還是失敗了⋯⋯

聽完一遍大致經過的夏緹亞點頭這樣說道。

「也就是說，要在不暴露是影耶的情況下把勇者打個半死不活，然後徹底滅了魔王，最後再只讓勇者回去就OK了吧？」
「雖然理解有點暴力，不過基本上就是這樣。理解這麼快幫大忙了」

夏緹亞雖然是個笨蛋，但頭腦並不差。只要說明一次就能把握事情的大概。

原來如此原來如此⋯⋯如此低語的夏緹亞瞇細眼，露出牙齒妖艷地微笑。

「──那麼，汝之願望為何？誕於吾之睿智的女兒唷──」
「啊，不用特地營造氣氛也沒關係。簡單來說就是和你契約對吧？魔力你要多少都會給你的。」
「難得人家想耍帥一下⋯⋯⋯嘛，就是這樣，要是就現在這樣的話連在現世一個星期都撐不過去。」
「上次只是隨便灌了點魔力，這次會認真幹的。要多少都拿去吧」
「是嗎？嘛，小影耶的魔力量也不用擔心。那我就盡情地吸取魔力嘍？」
「啊啊，不用客氣」

我答應的瞬間，夏緹亞毫不猶豫地咬上我的脖子。
魔力從身體中不斷被吸走，背部傳來一陣又一陣的虛脫感。

「呀啊啊啊啊！？」
「唔嘻唔唔嘻唔呀嘻（只能吸取一次所以不要亂動）」
「你，咦，你這傢伙，之前不是只要手碰魔法陣就好了嗎」
「唔嘁呴嘻唔唔噗唔唔（這麼大量的魔力一點一點吸收等結束都天黑了）」
「絶、絶對一半以上是騙人的！呀，不要趁亂亂摸人家！」

一瞬間想把夏緹亞給推開，但想到要是中斷後就不能在讓渡魔力的話就還是做罷了。
一邊忍耐這股感覺，一邊等待夏緹亞魔力吸收完畢。

「⋯⋯吶、吶，已經可以了嗎？」
「恩～⋯⋯」
「⋯⋯喂，其實已經結束了吧？」
「舔⋯⋯」
「舔什麼啊這個笨蛋！」
「唔噗！」

我抓住夏緹亞的手腕用巴投的要領將他摔出去。

「痛痛痛⋯⋯⋯真是的，人家只是小小惡作劇一下嘛⋯⋯」
「果然是在惡作劇嗎⋯⋯⋯然後呢，也讓渡不少魔力給你了，這次可以在地上多久？」
「⋯⋯多久呢？我也是第一次獲得這麼多魔力，雖然不清楚正確的時間⋯⋯但三個月應該沒問題吧？」
「這樣就夠了。那就出發前往神光國的障壁塔吧」
「欸，神光國的障壁塔？」
「沒錯。有什麼問題嗎？」
「不是啊，其實⋯⋯」

※

散播死亡的不死者英雄哈魯梅，正在障壁塔最上層俯視著神光國的街道。
雖然窗外射入的陽光會燒傷不死者，但這點縫隙射入的陽光對被強化的他根本沒有效果。

「雖然夏緹亞大人不知道去到哪裡了，不過只要有這力量的話，毀掉眼前所有市街也是易如反掌⋯⋯」

哈魯梅過去是某個國家中被人懼怕的狂戰士五兄弟的四男。死後化身不死者的他們帶著比生前更強大的狂氣蹂躪世界。然而與他們正反對的就是神光國的神聖騎士。兄弟中有三人被神聖騎士給滅掉，活下來的只有哈魯梅和哥哥，兩人一心只想著如何對神光國復仇而蓄積著力量。然而要打倒被稱為不死者天敵的神聖騎士可不簡單。
這樣的哈魯梅從魔王手上獲得了力量，還被給予魔人夏緹亞的召喚方法作為進一步獲取力量的方法。

哈魯梅現在向著地上構築巨大的魔法陣。匹敵魔王一擊的極大魔法就要被擊出。

「哈哈哈哈哈！這樣這個可恨的國家也就完了！已經沒有人可以阻⋯⋯！」

一道陽光射入身體，被燒傷的灼熱打斷他說話。

⋯⋯看來似乎是地面上的鏡子還是什麼反射陽光。應該不是刻意狙擊哈魯梅，但還是令人氣憤。

「哼，算了⋯⋯⋯毀滅這個國家後，再用暗之結界讓這裡永不見天日⋯⋯唔！」

這次從別的方向射來光線。這種程度雖然不會受多大的傷，但果然還是令人不爽。
趕快滅掉這個國家吧，如此心想加速魔法構築的哈魯梅又被光射到。

「可惡──啊，唔，咕！」

許多光束刺向哈魯梅。毫無疑問，這個光是來自某人的攻擊。

「可恨的──咕，啊，唔──啊啊，《深淵之帳》！」

展開暗黑的防御結界。不然這樣下去極大魔法實在也沒辦法順利構築。
鬆一口氣的哈魯梅，透過結界看到光芒在閃耀。

「不過，在有結界的現在，區區日光──恩？」

閃耀光芒的不是一處。數十、數百、數千⋯⋯不對，在這之上數量的光芒，不只地上連空中的所有地方都在閃耀。

「什、什麼⋯⋯！」

光芒收束在一點──連著障壁塔最上層，哈魯梅被燒的灰都不剩。

※

結論，太陽無敵。

變回原本的樣子，戴上假面和大衣的我，用轉移魔法陣讓拿著魔法鏡子的大量無人機回到倉庫。
雖然是模仿阿基米德逸事的兵器，但本來其實沒有這樣的威力。是因為增加光量的魔法以及異世界特有的金屬等才有現在的威力。

嘛，就算不做這種事也能普通打倒，但正好可以拿來測試魔法以外的強力遠距離攻擊。雖然有陽光的地方是必需條件，但可以指定效果範圍這點可以說是相當不錯。不過這有可能會蒸發到連復活都辦不到的地步所以也有點危險。威力調整應該會是個好問題。

「不過這實在是太大手筆了⋯⋯⋯普通地用大口徑的狙擊槍是不是更好啊？不過要是物理攻擊感覺又會被艾伊西亞的星魔法給反射⋯⋯可惡，一點也好，當初就該稍微看下雷射相關的輸」
「⋯⋯陰矢你啊，只要不手下留情就是強到沒朋友的地步呢」
「啊，雖然現在旁邊沒人，但這副打扮的名字是伊亞所以之後就叫這個名字」
「知道了知道了。攻略了障壁塔是好，但接下來呢？」
「作戰會議」

用改造魔法修復最上層時，我順便從地板創造出椅子和桌子。
坐上位置脫下假面，我面對著夏緹亞。

「我想，上次失敗的原因應該就是在我作為反派光明正大登場的緣故」
「是嗎⋯⋯⋯總感覺有什麼更加根本上的錯誤⋯⋯⋯」
「沒有，就是這樣。所以這次的作戰很簡單──作為伙伴，從背後捅他一刀」
「太單純了吧⋯⋯⋯這樣真的能行？」
「實際上我也想過很多，但多半能行。而且說到英雄的最後就是伙伴的背叛了。不過這不可能讓影耶去做。所以──」
「──就輪到我出場了，對吧？」

就是這樣。能和我不分上下的夏緹亞，強度自然無可挑剔，而且過一陣子就要回魔界了，所以就算沾染犯罪也不是什麼大問題。而且她好像本來就幹過許多壊事了。

「不不不，我只是被召喚出來，所以沒辦法才回應召喚者的願望。而且幾乎都是授予力量沒有自己下過手。嘛這也是因為我是魔人的緣故。」
「⋯⋯這樣的話就有點懸念了。你不會要殺人之前才開始怕吧？」
「啊哈哈，不要把我當作陰矢那種膽小鬼」
「喂」
「反正你都能復活不是嗎？不過不能和小影耶一起出門倒是有點困擾」
「⋯⋯之後我會再去做變裝用的道具啦」

之後我和夏緹亞進行詳細的計劃討論，並準備作戰所需要的一切東西。
以上次的失敗為教訓，我準備了各式各樣的計劃，並不只障壁塔，在神光國的各處都設下了陷阱。
光彌的人體情報在上次的戰鬥中已經拿到手了，所以不會對光彌以外的人發動。

再加上夏緹亞的裝備也準備好了。
雖然普通的樣子就很強，所以幾乎都是變裝用的裝備。嘛就算暴露也總有辦法的，所以稍微改變印象就夠了吧。

「吶吶，這個如何？看起來是不是很聰明？適合我嗎？」
「眼鏡嗎⋯⋯⋯恩，不說話的話就很適合」
「喂喂你這是什麼意思」

賢者風裝備、盜賊風裝備、騎士風、忍者風、魔女、舞娘⋯⋯⋯我做出各種各樣的（Cos）裝備。雖然我也沒資格說別人，但這完全就是在玩Cosplay嘛。
最後，她選了和影耶算是成對的白色系的重戰士裝備。明明是魔人卻一股神聖感。基本上只重視外表，看著很厚重但其實和外觀不同完全沒有防御能力。少面姑且施有魔法，但效果都是方便動作變得更舒服更好穿之類的。

總之，這樣決鬥的準備就完成了。

變身成影耶的我，帶上夏緹亞，前往神光國郊外與光彌約好要會合的地方。