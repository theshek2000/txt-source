---
LocalesID: 萬年Ｄ等級的中年冒險者
LocalesURL: https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%90%AC%E5%B9%B4%EF%BC%A4%E7%AD%89%E7%B4%9A%E7%9A%84%E4%B8%AD%E5%B9%B4%E5%86%92%E9%9A%AA%E8%80%85.ts
---
__TOC__

[萬年Ｄ等級的中年冒險者](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%90%AC%E5%B9%B4%EF%BC%A4%E7%AD%89%E7%B4%9A%E7%9A%84%E4%B8%AD%E5%B9%B4%E5%86%92%E9%9A%AA%E8%80%85.ts)  
總數：14／14

# Pattern

## 維妮婭

### patterns

- `維尼婭`

## 維納斯

### patterns

- `ウェヌス`
- `威納斯`

## 艾莉婭

### patterns

- `アリア`
- `艾莉亞`

## 盧卡斯

### patterns

- `ルーカス`

## 聖秘銀

### patterns

- `聖銀ミスリル`

## 聖德古拉

### patterns

- `セントグラ`
- `聖德古拉`
- `聖德拉`
- `瑟特菈`

## 珂露雪

### patterns

- `クルシェ`
- `庫魯修`
- `珂露雪`

## 菲奧拉

### patterns

- `フィオラ`
- `菲奧拉`
- `菲恩娜`

## 萊亞

### patterns

- `レア`
- `莉亞`

## 彌諾陶洛斯

### patterns

- `彌諾陶洛斯`
- `米諾塔洛斯`

## 龍娜

### patterns

- `リョナ`

## 豬頭帝

### patterns

- `オークエンペラー`
- `奧克皇帝`

## 塞蕾斯蒂妮

### patterns

- `セレスティーネ`
- `塞賽雷斯蒂寧`
- `塞蕾斯蒂寧`

## 塞蕾斯

### patterns

- `セレス`
- `賽雷斯`


